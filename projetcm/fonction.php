<?php 


function select($req)
{
	$mysqli = new mysqli('localhost','root','','projetcm');
	if($mysqli->connect_error) 
	{
		die('Un problème est survenu lors de la tentative de connexion à la BDD : ' . $mysqli->connect_error);
	}
	$resultat = $mysqli->query($req); 
	if (!$resultat)
	{
		die("Erreur sur la requete sql.<br />Message : " 
		. $mysqli->error . "<br />Code: " . $req);
	}

 
	return $resultat;
}







function CreateCart()
{
   if (!isset($_SESSION['panier']))
   {
	  $_SESSION['panier']=array();
	  $_SESSION['panier']['id'] = array();
      $_SESSION['panier']['titre'] = array();
      $_SESSION['panier']['description'] = array();
      $_SESSION['panier']['photo'] = array();
	  $_SESSION['panier']['prix'] = array();
	  $_SESSION['panier']['stock'] = array();
	  
   }
   return true;
}











function AddProductToCart($id,$titre ,$quantite ,$prix)
{

	CreateCart();

	if (isset($_SESSION['panier']))
	{
			$position=array_search($id, $_SESSION['panier']['id']);

			if($position!== false)
			{
	
				$_SESSION['panier']['quantite'][$position]+=$quantite; 
			}
		
			else
			{
			array_push($_SESSION['panier']['id'],$id);
			array_push($_SESSION['panier']['titre'],$titre);
			array_push($_SESSION['panier']['quantite'],$quantite);
			array_push($_SESSION['panier']['prix'],$prix);

			}
	}

}



 function MontantGlobal(){
	 
	 $c=count($_SESSION['panier']['id']);
	 $total=0;
	 for($i=0; $i<$c;$i++){
		 $total+=$_SESSION['panier']['quantite'][$i]*$_SESSION['panier']['prix'][$i];
	 }
	 return $total;
 }






 function sup($id)
 {
	
	if (CreateCart())
	{
	   
	   $tmp=array();
	   $tmp['id'] = array();
	   $tmp['titre'] = array();
	   $tmp['quantite'] = array();
	   $tmp['prix'] = array();
 
	   for($i = 0; $i < count($_SESSION['panier']['id']); $i++)
	   {
		  if ($_SESSION['panier']['id'][$i] !== $id)
		  {
			 array_push( $tmp['id'],$_SESSION['panier']['id'][$i]);
			 array_push( $tmp['titre'],$_SESSION['panier']['titre'][$i]);
			 array_push( $tmp['quantite'],$_SESSION['panier']['quantite'][$i]);
			 array_push( $tmp['prix'],$_SESSION['panier']['prix'][$i]);
		  }
 
	   }

	   $_SESSION['panier'] =  $tmp;

	   unset($tmp);
	}
	else
	{
		echo "An Error Has Occured";

	}
 }
 






 function recup($i)
 {
 	$_SESSION['indice']=$i;
 }
 
 



 
function removecart($idtoremove)
{
   $position_produit = array_search($idtoremove,  $_SESSION['panier']['id']);
   if ($position_produit !== false)
    {
      array_splice($_SESSION['panier']['id'], $position_produit, 1);
      array_splice($_SESSION['panier']['titre'], $position_produit, 1);
      array_splice($_SESSION['panier']['quantite'], $position_produit, 1);
      array_splice($_SESSION['panier']['prix'], $position_produit, 1);
   }
}



?>