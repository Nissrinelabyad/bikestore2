CREATE TABLE `commande` (
  `id` int(11) NOT NULL,
  `montant` varchar(20) NOT NULL,
  `date` datetime NOT NULL,
  `etat` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE `membre` (
  `idmembre` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `datenaissance` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE `produits` (
  `id` int(11) NOT NULL,
  `titre` varchar(20) NOT NULL,
  `description` varchar(250) NOT NULL,
  `photo` varchar(20) NOT NULL,
  `prix` varchar(20) NOT NULL,
  `stock` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


ALTER TABLE `commande`
  ADD PRIMARY KEY (`id`);
  
  ALTER TABLE `membre`
  ADD PRIMARY KEY (`idmembre`),
  ADD UNIQUE KEY `username` (`username`);
  
  ALTER TABLE `produits`
  ADD PRIMARY KEY (`id`);
  
  
  ALTER TABLE `commande`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
  
  ALTER TABLE `membre`
  MODIFY `idmembre` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
  
  
  ALTER TABLE `produits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;


  
  
  